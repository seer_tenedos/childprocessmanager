﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChildProcessManager
{
    public class Response<T>
    {
        public T Value { get; set; }
        public bool IsError => Error != null;
        public Error Error { get; set; }
        public TimeSpan IpcTimeTaken { get; set; }
    }
}
