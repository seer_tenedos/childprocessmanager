﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChildProcessManager
{
    public class CommandsAndConst
    {
        public const byte Ping = 255;
        public const byte Shutdown = 254;
        public const byte ShuttingDown = 253;

        public const string EnvIpcIdName = "ChildProcesses.IpcId";
        public const string EnvAutoAttachDebuggerName = "ChildProcesses.AutoAttachDebugger";
    }
}
