﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ChildProcessManager
{
    /// <summary>
    ///     The process start event args.
    /// </summary>
    public class ProcessStartEventArgs : EventArgs
    {
        /// <summary>
        ///     Gets or sets the manager.
        /// </summary>
        public ProcessManager Manager { get; set; }

        /// <summary>
        ///     Gets the process start info.
        /// </summary>
        public ProcessStartInfo ProcessStartInfo { get; internal set; }

        /// <summary>
        ///     Gets the started process.
        /// </summary>
        public ChildProcessInfo StartedProcessInfo { get; internal set; }

    }
}
