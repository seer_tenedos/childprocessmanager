﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace ChildProcessManager
{
    /// <summary>
    ///     The process state changed event handler.
    /// </summary>
    /// <param name="sender">
    ///     The sender.
    /// </param>
    /// <param name="e">
    ///     The e.
    /// </param>
    public delegate void ProcessStateChangedHandler(ChildProcessStateChangedEnum state, string extraInfo);

    public class ChildProcess : ISource, IDisposable
    {
        private object lockObj = new object();
        /// <summary>
        /// The watchdog timer.
        /// </summary>
        private Timer _watchdog;

        public string IpcId { get; }
        public int ProcessId { get; }
        protected IpcInstance Ipc { get; }

        /// <summary>
        ///     The last time alive.
        /// </summary>
        public DateTime LastTimeAlive { get; private set; }

        public RequestProcessors Processors { get; }
        public int WatchDogTimeoutInSeconds { get; }

        /// <summary>
        ///     The watchdog timeout.
        /// </summary>
        public bool WatchdogTimeout => LastTimeAlive.AddSeconds(WatchDogTimeoutInSeconds) < DateTime.Now;

        #region Delegates
        /// <summary>
        ///     The process state changed.
        /// </summary>
        private readonly ProcessStateChangedHandler _processStateChanged;

        #endregion

        public ChildProcess(ProcessStateChangedHandler processStateChanged, RequestProcessors processors, int watchDogTimeoutInSeconds = 60)
        {
            if (!IsChildProcess())
            {
                throw new InvalidOperationException("This is not a child process!");
            }

            _processStateChanged = processStateChanged ?? throw new ArgumentNullException(nameof(processStateChanged));
            WatchDogTimeoutInSeconds = watchDogTimeoutInSeconds;

            IpcId = Environment.GetEnvironmentVariable(CommandsAndConst.EnvIpcIdName);
            if (string.IsNullOrWhiteSpace(IpcId))
            {
                throw new InvalidOperationException($"Environment variable '{CommandsAndConst.EnvIpcIdName}' was not set.");
            }

            ProcessId = Process.GetCurrentProcess().Id;

            //attach debugger if requested
            AutoAttachDebuggerIfRequested();

            if (processors == null)
            {
                Processors = new RequestProcessors(this);
            }
            else
            {
                if (processors.GetRegisteredCommands().Any(x => x >= 240))
                {
                    throw new ArgumentException(nameof(processors), $"A processor with a command above 239 was registered. Commands 240 and above are reserved.");
                }
                Processors = new RequestProcessors(processors, this);
            }
            
            RegisterInternalResponseProcessors();

            LastTimeAlive = DateTime.Now;
            Ipc = new IpcInstance(IpcId, Processors);

            if (!SendKeepAlive(100000))
            {
                throw new Exception("IPC connection failed on startup.");
            }
            
            _watchdog = new Timer(x => OnWatchdog(), null, TimeSpan.FromSeconds(WatchDogTimeoutInSeconds), TimeSpan.FromSeconds(WatchDogTimeoutInSeconds / 2));
        }

        private void RegisterInternalResponseProcessors()
        {
            Processors.RegisterRequestProcessor<string, string>(CommandsAndConst.Ping, (source, request) =>
            {
                LastTimeAlive = DateTime.Now;
                return new Response<string>() {Value = $"Pong from {IpcId}"};
            });

            Processors.RegisterRequestWithNoResponseProcessor<string>(CommandsAndConst.ShuttingDown,
                (source, request) =>
                {
                    _processStateChanged(ChildProcessStateChangedEnum.ParentExited, $"Parent message: {request}");
                });

            Processors.RegisterRequestWithNoResponseProcessor<string>(CommandsAndConst.Shutdown,
                (source, request) =>
                {
                    _processStateChanged(ChildProcessStateChangedEnum.ShutdownRequested,
                        $"Parent message: {request}");
                });
        }

        public Response<TResponse> SendRequest<TRequest, TResponse>(byte command, TRequest request, int timeoutMs = 30000)
        {
            if (command >= 240)
            {
                throw new ArgumentOutOfRangeException(nameof(command), command, $"Command was {command} and must be less than 240. Commands 240 and above are reserved.");
            }
            return Ipc.SendRequest<TRequest, TResponse>(command, request, timeoutMs);
        }

        /// <summary>
        /// Send a request and expects no response
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <param name="command"></param>
        /// <param name="request"></param>
        /// <returns>true if request was sent</returns>
        public bool SendRequestWithoutResponse<TRequest>(byte command, TRequest request)
        {
            if (command >= 240)
            {
                throw new ArgumentOutOfRangeException(nameof(command), command, $"Command was {command} and must be less than 240. Commands 240 and above are reserved.");
            }
            return Ipc.SendRequestWithoutResponse<TRequest>(command, request); ;
        }

        /// <summary>
        /// The on watchdog.
        /// </summary>
        protected void OnWatchdog()
        {
            SendKeepAlive();
            if (WatchdogTimeout)
            {
                _processStateChanged(ChildProcessStateChangedEnum.WatchdogTimeout, null);
            }
        }

        /// <summary>
        /// Send keep alive
        /// </summary>
        /// <returns>True if we get success response</returns>
        public bool SendKeepAlive(int timeoutMs = 2000)
        {
            var response = Ipc.SendRequest<string, string>(CommandsAndConst.Ping, $"Ping from {IpcId}", timeoutMs);
            if (response == null || response.IsError)
            {
                _processStateChanged(ChildProcessStateChangedEnum.PingFailed, response?.Error?.ToString());
                return false;
            }
            else
            {
                LastTimeAlive = DateTime.Now;
                return true;
            }
        }

        public static bool IsChildProcess()
        {
            return !string.IsNullOrWhiteSpace(Environment.GetEnvironmentVariable(CommandsAndConst.EnvIpcIdName));
        }

        private void AutoAttachDebuggerIfRequested()
        {
            var envValue = Environment.GetEnvironmentVariable(CommandsAndConst.EnvAutoAttachDebuggerName);
            bool attach = false;
            if (!string.IsNullOrWhiteSpace(envValue) && bool.TryParse(envValue, out attach) && attach)
            {
                Debugger.Launch();
                SpinWait.SpinUntil(()=>Debugger.IsAttached, TimeSpan.FromSeconds(30));
                //if (Debugger.IsAttached)
                //{
                //    //give debugger time to start up fully
                //    Thread.Sleep(200);
                //}
            }
        }

        public void Dispose()
        {
            try
            {
                Ipc?.SendRequestWithoutResponse(CommandsAndConst.ShuttingDown, $"Dispose called on {IpcId}");
            }
            catch { }

            _watchdog?.Dispose();
            Ipc?.Dispose();
        }
    }
}
