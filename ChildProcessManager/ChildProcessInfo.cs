﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The Child Process represents the child process on the parent. It is manages by a <see cref="ProcessManager" />
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Linq;

namespace ChildProcessManager
{
    /// <summary>
    ///     The Child Process represents the child process on the parent. It is manages by a <see cref="ProcessManager" />
    /// </summary>
    public class ChildProcessInfo: ISource, IDisposable
    {
        #region Fields
        private object lockObj = new object();
        
        public string IpcId { get; private set; }
        protected IpcInstance Ipc { get; private set; }
        public int ProcessId { get; private set; }

        public RequestProcessors Processors { get; }

        /// <summary>
        ///     The last time alive. (last time we got something from the child)
        /// </summary>
        public DateTime? LastTimeAlive { get; private set; }

        public DateTime CreatedAt { get; }

        /// <summary>
        ///     The watchdog timeout. If LastTimeAlive not set use creation date.
        /// </summary>
        public bool WatchdogTimeout => (LastTimeAlive?? CreatedAt).AddSeconds(Manager.WatchDogTimeoutInSeconds) < DateTime.Now;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ChildProcessInfo" /> class.
        /// </summary>
        public ChildProcessInfo(RequestProcessors processors)
        {
            IpcId = Guid.NewGuid().ToString("N");
            CreatedAt = DateTime.Now;
            if (processors == null)
            {
                Processors = new RequestProcessors(this);
            }
            else
            {
                if (processors.GetRegisteredCommands().Any(x => x >= 240))
                {
                    throw new ArgumentException(nameof(processors), $"A processor with a command above 239 was registered. Commands 240 and above are reserved.");
                }
                Processors = new RequestProcessors(processors, this);
            }
            RegisterInternalResponseProcessors();
            Ipc = new IpcInstance(IpcId, Processors);
        }
        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets AdditionalData.
        /// </summary>
        public object AdditionalData { get; set; }

        /// <summary>
        ///     Gets or sets Manager.
        /// </summary>
        public ProcessManager Manager { get; internal set; }

        /// <summary>
        ///     Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        public bool ProcessIsAlive => Process != null && !Process.HasExited;

        /// <summary>
        ///     Gets Process.
        /// </summary>
        public Process Process { get; private set; }

        private void RegisterInternalResponseProcessors()
        {
            Processors.RegisterRequestProcessor<string, string>(CommandsAndConst.Ping, (source, request) =>
            {
                LastTimeAlive = DateTime.Now;
                return new Response<string>() { Value = "Pong from Server" };
            });
            Processors.RegisterRequestWithNoResponseProcessor<string>(CommandsAndConst.ShuttingDown, (source, request) =>
            {
                Manager.RaiseProcessStateChangedEvent(this, ManagerProcessStateChangedEnum.ChildShutdown, $"Child message: {request}");
            });
        }

        public Response<TResponse> SendRequest<TRequest, TResponse>(byte command, TRequest request, int timeoutMs = 30000)
        {
            if (command >= 240)
            {
                throw new ArgumentOutOfRangeException(nameof(command), command, $"Command was {command} and must be less than 240. Commands 240 and above are reserved.");
            }
            return Ipc.SendRequest<TRequest, TResponse>(command, request, timeoutMs);
        }

        /// <summary>
        /// Send a request and expects no response
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <param name="command"></param>
        /// <param name="request"></param>
        /// <returns>true if request was sent</returns>
        public bool SendRequestWithoutResponse<TRequest>(byte command, TRequest request)
        {
            if (command >= 240)
            {
                throw new ArgumentOutOfRangeException(nameof(command), command, $"Command was {command} and must be less than 240. Commands 240 and above are reserved.");
            }
            return Ipc.SendRequestWithoutResponse<TRequest>(command, request); ;
        }


        public bool SendKeepAlive()
        {
            //should have received an message if client up
            if (ProcessIsAlive && Ipc?.LastMessageReceivedAt != null)
            {
                var result = Ipc.SendRequest<string, string>(CommandsAndConst.Ping, "Server Ping", 1000);
                if (!result.IsError)
                {
                    LastTimeAlive = DateTime.Now;
                    return true;
                }
            }
            return false;
        }

        public void SendParentShuttingDown()
        {
            //should have received an message if client up


            if (ProcessIsAlive && Ipc?.LastMessageReceivedAt != null)
            {
                Ipc.SendRequestWithoutResponse<string>(CommandsAndConst.ShuttingDown, "Parent is being disposed.");
            }
        }

        public void Shutdown(bool forceKillProcess = false)
        {
            lock (lockObj)
            {
                //TODO:make sure this never throws an exception
                if (forceKillProcess && ProcessIsAlive)
                {
                    Process.Kill();
                }

                //should have received an message if client up
                if (ProcessIsAlive && Ipc?.LastMessageReceivedAt != null)
                {
                    var result = Ipc.SendRequestWithoutResponse<string>(CommandsAndConst.Shutdown, null);
                    if (result)
                    {
                        //wait up to 15 seconds to exit before we force kill it
                        if (!Process.WaitForExit(15000))
                        {
                            Process.Kill();
                        } 
                    }
                    else
                    {
                        //TODO:Log that we failed to send
                        Process.Kill();
                    }
                }
                else if(ProcessIsAlive)
                {
                    Process.Kill();
                }

                Ipc?.Dispose();
                Ipc = null;
                Manager.RaiseProcessStateChangedEvent(this, ManagerProcessStateChangedEnum.ChildShutdown, "Shutdown successful");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     The post start init.
        /// </summary>
        protected internal virtual void PostStartInit()
        {
            Process.BeginOutputReadLine();
            Process.BeginErrorReadLine();
            ProcessId = Process.Id;
        }

        /// <summary>
        /// The pre start init.
        /// </summary>
        /// <param name="manager">
        /// The manager.
        /// </param>
        /// <param name="process">
        /// The process.
        /// </param>
        protected internal virtual void PreStartInit(ProcessManager manager, Process process)
        {
            Manager = manager;
            Process = process;
            Process.OutputDataReceived += Process_OutputDataReceived;
            Process.ErrorDataReceived += Process_ErrorDataReceived;
        }

        /// <summary>
        /// The process_ error data received.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Process_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Manager.RaiseProcessStateChangedEvent(this, ManagerProcessStateChangedEnum.StandardErrorMessage, e.Data);
        }

        /// <summary>
        /// The process_ output data received.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Manager.RaiseProcessStateChangedEvent(this, ManagerProcessStateChangedEnum.StandardOutputMessage, e.Data);
        }

        #endregion

        public void Dispose()
        {
            lock (lockObj)
            {
                Ipc?.Dispose();
                Ipc = null;
                if (ProcessIsAlive)
                {
                    Process?.Kill();
                    Manager.RaiseProcessStateChangedEvent(this, ManagerProcessStateChangedEnum.ChildShutdown, "Child shutdown due to dispose");
                }
                Process?.Dispose();
                Process = null;
            }
        }
    }
}