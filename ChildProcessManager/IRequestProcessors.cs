﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChildProcessManager
{
    public interface IRequestProcessors
    {
        Processor GetProcessor(byte command);
    }
}
