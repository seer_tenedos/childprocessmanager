﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessStateChangedEnum.cs" company="Maierhofer Software, Germany">
//   Copyright 2012 by Maierhofer Software, Germany
// </copyright>
// <summary>
//   TODO: Update summary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ChildProcessManager
{
    /// <summary>
    ///     Manager side Process State changes
    /// </summary>
    public enum ManagerProcessStateChangedEnum
    {
        /// <summary>
        ///     The Child IPC Callback Channel is available
        /// </summary>
        IpcChannelAvail, 

        /// <summary>
        ///     Text is received from Child Process Standard Output
        /// </summary>
        StandardOutputMessage, 

        /// <summary>
        ///     Text is received from Child Process Standard Error
        /// </summary>
        StandardErrorMessage, 

        /// <summary>
        ///     A Child Process has Exited
        /// </summary>
        ChildExited,

        /// <summary>
        ///     A Child Process failed a Ping
        /// </summary>
        ChildPingFailed,

        /// <summary>
        ///     The Process don't send Watchdog notifications
        /// </summary>
        WatchdogTimeout, 

        /// <summary>
        ///     The Child Process Should Shutdown
        /// </summary>
        ChildShutdown
    }

    /// <summary>
    ///     Child side Process State changes
    /// </summary>
    public enum ChildProcessStateChangedEnum
    {
        /// <summary>
        ///     The Parent Process has Exited
        /// </summary>
        ParentExited,

        /// <summary>
        ///     The watchdog timed out so likely managing process has died
        /// </summary>
        WatchdogTimeout,

        /// <summary>
        ///     Ping to managing process failed.
        /// </summary>
        PingFailed,

        /// <summary>
        ///     The Child Process Should Shutdown
        /// </summary>
        ShutdownRequested
    }
}