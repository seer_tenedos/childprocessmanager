﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChildProcessManager
{
    public class Processor
    {
        private Func<ISource, object, Response<object>> _requestProcessor;
        public ISource Source { get; }
        public Type RequestType { get; }

        public Processor(Type requestType, Func<ISource, object, Response<object>> requestProcessor, ISource source)
        {
            RequestType = requestType;
            _requestProcessor = requestProcessor;
            Source = source;
        }

        public Processor DuplicateForNewSource(ISource source)
        {
            return new Processor(RequestType, _requestProcessor, source);
        }
       
        public Response<object> RequestProcessor(object request)
        {
            return _requestProcessor(Source, request);
        }

    }
}
