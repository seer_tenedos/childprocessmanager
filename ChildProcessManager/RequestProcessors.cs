﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChildProcessManager
{
    public class RequestProcessors: IRequestProcessors
    {
        private readonly ISource _source;
        private readonly ConcurrentDictionary<byte, Processor> _requestProcessors = new ConcurrentDictionary<byte, Processor>();

        public RequestProcessors()
        {
            _source = null;
        }

        public RequestProcessors(ISource source):this()
        {
            _source = source;
        }

        public RequestProcessors(RequestProcessors copyRequestProcessors):this()
        {
            Register(copyRequestProcessors);
        }
        public RequestProcessors(RequestProcessors copyRequestProcessors, ISource source) : this(source)
        {
            Register(copyRequestProcessors);
        }

        public void Register(RequestProcessors copyRequestProcessors)
        {
            foreach (var processor in copyRequestProcessors._requestProcessors)
            {
                if (!_requestProcessors.TryAdd(processor.Key, processor.Value.DuplicateForNewSource(_source)))
                {
                    throw new Exception($"Command '{processor.Key}' already registered to another processor.");
                }
            }
        }

        public void RegisterRequestProcessor<TResponse, TRequest>(byte command, Func<ISource, TRequest, Response<TResponse>> requestProcessor) where TRequest : class
        {

            Type type = typeof(TRequest);
            Func<ISource, object, Response<object>> wrapper = (source, req) =>
            {
                try
                {
                    var result = requestProcessor(source, req as TRequest);
                    return new Response<object>() { Error = result.Error, Value = result.Value };
                }
                catch (Exception e)
                {
                    return new Response<object>() { Error = Error.GenerateError(ErrorType.UnhandledException, e) };
                }
            };

            if (!_requestProcessors.TryAdd(command, new Processor(type, wrapper, _source)))
            {
                throw new Exception($"Command '{command}' already registered to another processor.");
            }
        }

        public void RegisterRequestWithNoResponseProcessor<TRequest>(byte command, Action<ISource, TRequest> requestProcessor) where TRequest : class
        {

            Type type = typeof(TRequest);
            Func<ISource, object, Response<object>> wrapper = (source, req) =>
            {
                try
                {
                    requestProcessor(source, req as TRequest);
                }
                catch (Exception ex)
                {
                    //TODO:add logging
                }
                return null;
            };

            if (!_requestProcessors.TryAdd(command, new Processor(type, wrapper, _source)))
            {
                throw new Exception($"Command '{command}' already registered to another processor.");
            }
        }

        public Processor GetProcessor(byte command)
        {
            _requestProcessors.TryGetValue(command, out var processor);
            return processor;
        }

        public byte[] GetRegisteredCommands()
        {
            return _requestProcessors.Keys.ToArray();
        }

    }
}
