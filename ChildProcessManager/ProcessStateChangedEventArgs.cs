﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessStateChangedEventArgs.cs" company="Maierhofer Software, Germany">
//   Copyright 2012 by Maierhofer Software, Germany
// </copyright>
// <summary>
//   TODO: Update summary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace ChildProcessManager
{
    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public class ProcessStateChangedEventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessStateChangedEventArgs"/> class.
        /// </summary>
        /// <param name="childProcessInfo">
        /// The child process.
        /// </param>
        /// <param name="action">
        /// The action.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public ProcessStateChangedEventArgs(ChildProcessInfo childProcessInfo, ManagerProcessStateChangedEnum action, string data)
        {
            this.ChildProcessInfo = childProcessInfo;
            this.Action = action;
            this.Data = data;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets Action.
        /// </summary>
        public ManagerProcessStateChangedEnum Action { get; private set; }

        /// <summary>
        ///     Gets ChildProcess.
        /// </summary>
        public ChildProcessInfo ChildProcessInfo { get; private set; }

        /// <summary>
        ///     Gets Data.
        /// </summary>
        public string Data { get; private set; }

        #endregion
    }
}