# ChildProcessManager
**** This is still Alpha ****

This is a library to manage child processes and communication with them.

This project is heavily based on http://www.crawler-lib.net/child-processes

Communication library is from https://github.com/hhblaze/SharmIPC/tree/master/Process1/SharmIpc but 
was copied into the project to get the latest version and to be able to debug comminitcion issues. The 
plan is to stream line the library for this particular usecase in the future to get better performance.

It was upgraded to use a different communication system and to be dotnet core compatable. 
The basic idea is the same but a lot of the classes were changed and the original license from the files moved to here
company="Maierhofer Software, Germany"
Copyright 2012 by Maierhofer Software, Germany